USE classic_models;

/* 1 */
SELECT customerName 
FROM customers 
WHERE country = "Philippines";

/* 2 */
SELECT contactLastName, contactFirstName
FROM customers
WHERE customerName = "La Rochelle Gifts";

/* 3 */
SELECT productName, MSRP
FROM products
WHERE productName = "The Titanic";

/* 4 */
SELECT firstName, lastName, email
FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";

/* 5 */
SELECT customerName
FROM customers
WHERE state IS NULL;

/* 6 */
SELECT firstName, lastName, email
FROM employees
WHERE lastName = "Patterson"
AND firstName = "Steve";

/* 7 */
SELECT customerName, country, creditLimit
FROM customers
WHERE NOT country = "USA"
AND creditLimit > 3000;

/* 8 */
SELECT customerName
FROM customers
WHERE customerName NOT LIKE "%a%";

/* 9 */
SELECT customerNumber
FROM orders
WHERE comments LIKE "%DHL%";

/* 10 */
SELECT productLine
FROM products
WHERE productDescription LIKE "%state of the art%";

/* 11 */
SELECT DISTINCT country
FROM customers;

/* 12 */
SELECT DISTINCT status
FROM orders;

/* 13 */
SELECT customerName, country
FROM customers
WHERE country = "USA"
OR country = "France"
OR country = "Canada";

/* 14 */
SELECT firstName, lastName, city AS officeCity
FROM employees 
JOIN offices ON employees.officeCode = offices.officeCode
WHERE city = "Tokyo";

/* 15 */
SELECT customerName
FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE firstName = "Leslie"
AND lastName = "Thompson";

/* 16 */
SELECT productName, customerName
FROM products
JOIN orderdetails ON products.productCode = orderDetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customerName = "Baane Mini Imports";

/* 17 */
SELECT 
  employees.firstName AS employeeFirstName, 
  employees.lastName AS employeeLastName, 
  customers.customerName, 
  offices.country
FROM employees
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
LEFT JOIN offices ON employees.officeCode = offices.officeCode
WHERE offices.country = customers.country;

/* 18 */
SELECT a.lastName, a.firstName
FROM employees a, employees b
WHERE a.reportsTo = b.employeeNumber
AND b.firstName = "Anthony"
AND b.lastName = "Bow";

/* 19 */
SELECT productName, MAX(MSRP)
FROM products;

/* 20 */
SELECT COUNT(*)
FROM customers
WHERE country = "UK";

/* 21 */
SELECT productLine, COUNT(*)
FROM products
GROUP BY productLine;

/* 22 */
SELECT employeeNumber, COUNT(salesRepEmployeeNumber)
FROM employees
LEFT JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
GROUP BY employeeNumber;

/* 23 */
SELECT productName, quantityInStock
FROM products
WHERE productLine = "planes"
AND quantityInStock < 1000;
